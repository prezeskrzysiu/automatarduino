<!DOCTYPE html>
<head>
  <?php include('elements/head.php'); ?>
</head>
<body style="background: url('pages/cubes.png');">
  <div class="container">
  <table>
    <caption>logs</caption>
    <thead>
      <tr>
        <th>id</th>
        <th>description</th>
        <th>module</th>
        <th>value</th>
        <th>date</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $Database = Database::getInstance();
  		$pdo = $Database->getPDO();
      $sql = $pdo->query("SELECT * FROM logs ORDER BY date DESC");
      foreach($sql as $row){
        echo '<tr><td>'.$row['id'].'</td><td>'.$row['description'].'</td><td>'.$row['module'].'</td><td>'.$row['value'].'</td><td>'.$row['date'].'</td></tr>';
      }
      ?>

    </tbody>
  </table>
  <iframe src="http://192.168.0.9"></iframe>
  </div>
</body>
</html>
