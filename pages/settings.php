<!DOCTYPE html>
<head>
  <?php include('elements/head.php'); ?>
</head>
<script>
function getStationSettings(id){
  $.ajax({
    type: "POST",
    url: "/ajax/settings.ajax.php",
    data: {id:id,settings:"station"},
    success: function(response){
      var data = jQuery.parseJSON(response);
      var html = '<label for="modal-station-settings" class="modal-close"></label><h3 class="section" id="dialog-title">'+data.name+'</h3>';
      html = html + '<div id="station-settings-info" style="margin-top:5px;padding:5px;"></div><div class="row">';
      html = html + '<div class="col-md-4"><span class="tooltip" aria-label="Nazwa stacji"><input type="text" id="settings-station-name" value="'+data.name+'"/></span></div>';
      html = html + '<div class="col-md-4"><span class="tooltip" aria-label="IP stacji"><input type="text" id="settings-station-ip" value="'+data.ip+'"/></span></div>';
      html = html + '<div class="col-md-4"><span class="tooltip" aria-label="Port stacji (domyÅlny to 80)"><input type="text" id="settings-station-port" value="'+data.port+'"/></span></div>';
      html = html + '</div>';
      html = html + '<button class="primary" onClick="editStation('+id+');">Edytuj</button>'
      $("#modal-station-settings-body").html(html);
    }
  });
}

function getModuleSettings(id){
  $.ajax({
    type: "POST",
    url: "/ajax/settings.ajax.php",
    data: {id:id,settings:"module"},
    success: function(response){
      var data = jQuery.parseJSON(response);
      var html = '<label for="modal-module-settings" class="modal-close"></label><h3 class="section" id="dialog-title">'+data.name+'</h3>';
      html = html + '<div id="module-settings-info" style="margin-top:5px;padding:5px;"></div><div class="row">';
      html = html + '<div class="col-md-12"><span class="tooltip" aria-label="Nazwa moduÅu"><input type="text" id="settings-module-name" value="'+data.name+'"/></span></div>';
      html = html + '<div class="col-md-12"><span class="tooltip" aria-label="PIN"><select id="settings-module-pin">';
      html = html + data.pinsHTML;
      html = html + '</select></span></div>';
      html = html + '</div>';
      html = html + '<button class="primary" onClick="editModule('+id+');">Edytuj</button>';
      $("#modal-module-settings-body").html(html);
    }
  });
}

function addStation(){
  var name = $("#station-name").val();
  var ip = $("#station-ip").val();
  var port = $("#station-port").val();
  var board = $("#station-board").val();
  $.ajax({
    type: "POST",
    url: "/ajax/station.ajax.php",
    data: {name:name,ip:ip,port:port,board:board,action:"add"},
    success: function(response){
      var data = jQuery.parseJSON(response);
      if(data.type=="success"){
         location.reload();
      } else {
        $("#station-add-info").addClass("section");
        $("#station-add-info").html(data.message);
      }
    }
  });
}

function editStation(id){
  var name = $("#settings-station-name").val();
  var ip = $("#settings-station-ip").val();
  var port = $("#settings-station-port").val();
  $.ajax({
    type: "POST",
    url: "/ajax/station.ajax.php",
    data: {id:id,name:name,ip:ip,port:port,action:"edit"},
    success: function(response){
      var data = jQuery.parseJSON(response);
      if(data.type=="success"){
         location.reload();
      } else {
        $("#station-settings-info").addClass("card");
        $("#station-settings-info").html(data.message);
      }
    }
  });
}

function deleteStation(id){
  var sure = confirm("Czy na pewno chcesz usunÄÄ stacje o ID "+id+" wraz z jej moduÅami i ustawieniami?");
  if(sure){
    $.ajax({
      type: "POST",
      url: "/ajax/station.ajax.php",
      data: {id:id,action:"delete"},
      success: function(response){
        var data = jQuery.parseJSON(response);
        if(data.type=="success"){
           location.reload();
        } else {
          alert(data.message);
        }
      }
    });
  }
}

function addModule(){
  var name = $("#module-name").val();
  var station = $("#module-station").val();
  var part = $("#module-part").val();
  var pin = $("#module-pin").val();
  $.ajax({
    type: "POST",
    url: "/ajax/module.ajax.php",
    data: {name:name,station:station,part:part,pin:pin,action:"add"},
    success: function(response){
      var data = jQuery.parseJSON(response);
      if(data.type=="success"){
         location.reload();
      } else {
        $("#module-add-info").addClass("section");
        $("#module-add-info").html(data.message);
      }
    }
  });
}

function editModule(id){
  var name = $("#settings-module-name").val();
  var pin = $("#settings-module-pin").val();
  $.ajax({
    type: "POST",
    url: "/ajax/module.ajax.php",
    data: {id:id,name:name,pin:pin,action:"edit"},
    success: function(response){
      var data = jQuery.parseJSON(response);
      if(data.type=="success"){
         location.reload();
      } else {
        $("#module-settings-info").addClass("card");
        $("#module-settings-info").html(data.message);
      }
    }
  });
}

</script>
<body style="background: url('pages/cubes.png');">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <a class="button" href="home">Strona gÅÃ³wna</a>
      </div>
      <div class="col-md-2">
        <label for="modal-module-add"><button class="tertiary"><span class="icon-share"></span>Dodaj moduÅ</button></label>
      </div>
      <div class="col-md-2">
        <label for="modal-station-add"><button class="tertiary"><span class="icon-home"></span>Dodaj stacje</button></label>
      </div>
    </div>
    <?php
      $showStations = '
        <table>
          <caption><span class="icon-home"></span> Stacje</caption>
          <thead>
            <tr>
              <th>ID</th>
              <th>Nazwa</th>
              <th>IP</th>
              <th>IloÅÄ moduÅÃ³w</th>
              <th>Stan stacji</th>
              <th>PÅytka</th>
              <th>Edycja</th>
            </tr>
          </thead>
          <tbody>
      ';
      $Database = Database::getInstance();
  		$pdo = $Database->getPDO();
      $sql = $pdo->query("SELECT * FROM stations");
      if($sql->rowCount() > 0){
        foreach($sql as $row){
          $Station = new Station;
          $Station->id = $row['id'];
          $Station->name = $row['name'];
          $Station->ip = $row['ip'];
          $Station->port = $row['port'];
          $Station->board = $row['board'];
          $showStations .= '
            <tr>
              <td data-label="ID">'.$Station->id.'</td>
              <td data-label="Nazwa">'.$Station->name.'</td>
              <td data-label="IP">'.$Station->ip.':'.$Station->port.'</td>
              <td data-label="IloÅÄ moduÅÃ³w">'.$Station->modulesCount().'</td>
              <td data-label="Stan stacji">';
          $showStations .= $Station->checkAvailability() ? '<span class="icon-rss"></span>' : '<span class="icon-alert"></span>';
          $showStations .= '
              </td>
              <td data-label="PÅytka">'.$Station->boardName().'</td>
              <td data-label="Edycja"><label for="modal-station-settings" onClick="getStationSettings('.$Station->id.');" style="cursor:pointer;"><span class="icon-settings"></span></label> <span style="cursor:pointer;" onClick="deleteStation('.$Station->id.');">X</span></td>
            </tr>
          ';
        }
        $showStations .= '
            </tbody>
          </table>
        ';
        echo $showStations;
      } else echo 'Brak stacji w bazie danych.';
      $sql->closeCursor();
      $showModules = '
        <table>
          <caption><span class="icon-share"></span> ModuÅy</caption>
          <thead>
            <tr>
              <th>ID</th>
              <th>Nazwa</th>
              <th>Stacja</th>
              <th>CzÄÅÄ</th>
              <th>PIN</th>
              <th>Stan</th>
              <th>Edycja</th>
            </tr>
          </thead>
          <tbody>
      ';
      $sql = $pdo->query("SELECT * FROM modules ORDER BY station");
      if($sql->rowCount() > 0){
        foreach($sql as $row){
          $Module = new Module;
          $Module->id = $row['id'];
          $Module->name = $row['name'];
          $Module->station = $row['station'];
          $Module->part = $row['part'];
          $Module->pin = $row['pin'];
          $Module->enable = $row['enable'];
          $showModules .= '
            <tr>
              <td data-label="ID">'.$Module->id.'</td>
              <td data-label="Nazwa">'.$Module->name.'</td>
              <td data-label="Stacja">'.$Module->stationName().' ('.$Module->station.')</td>
              <td data-label="CzÄÅÄ">'.$Module->partName().' ('.$Module->part.') - '.$Module->partType().'</td>
              <td data-label="PIN">'.$Module->pinName().' ('.$Module->pin.')</td>
              <td data-label="Stan">';
          $showModules .= $Module->enable ? 'WÅÄczony' : 'WyÅÄczony';
          $showModules .='
              </td>
              <td data-label="Edycja"><label for="modal-module-settings" onClick="getModuleSettings('.$Module->id.');" style="cursor:pointer;"><span class="icon-settings"></span></label> <span style="cursor:pointer;">X</span></td>
            </tr>
          ';
        }
        $showModules .= '
            </tbody>
          </table>
        ';
        echo $showModules;
      } else echo 'Brak moduÅÃ³w w bazie danych.';
    ?>
  </div>
</body>
<input type="checkbox" id="modal-station-settings" class="modal">
<div role="dialog" aria-labelledby="dialog-title">
  <div id="modal-station-settings-body" class="card fluid">
  </div>
</div>

<input type="checkbox" id="modal-module-settings" class="modal">
<div role="dialog" aria-labelledby="dialog-title">
  <div id="modal-module-settings-body" class="card fluid">
  </div>
</div>

<input type="checkbox" id="modal-station-add" class="modal">
<div role="dialog" aria-labelledby="dialog-title">
  <div class="card large">
    <label for="modal-station-add" class="modal-close"></label>
    <h3 class="section" id="dialog-title">Dodaj stacje</h3>
    <div id="station-add-info"></div>
    <label for="station-name">Nazwa:</label>
    <input type="text" id="station-name" placeholder="Nazwa dla stacji"/>
    <label for="station-ip">IP:</label>
    <input type="text" id="station-ip" placeholder="IP stacji"/>
    <label for="station-port">Port:</label>
    <input type="text" id="station-port" value="80" placeholder="Port stacji"/>
    <label for="station-board">PÅytka:</label>
    <select id="station-board">
    <?php
      $sql = $pdo->query("SELECT id,name FROM boards");
      $showBoards = "";
      foreach($sql as $row) $showBoards .= '<option value="'.$row['id'].'">'.$row['name'].'</option>';
      echo $showBoards;
    ?>
    </select>
    <button class="tertiary" onClick="addStation();">Dodaj</button>
  </div>
</div>

<input type="checkbox" id="modal-module-add" class="modal">
<div role="dialog" aria-labelledby="dialog-title">
  <div class="card large">
    <label for="modal-module-add" class="modal-close"></label>
    <h3 class="section" id="dialog-title">Dodaj moduÅ</h3>
    <div id="module-add-info"></div>
    <label for="module-name">Nazwa:</label>
    <input type="text" id="module-name" placeholder="Nazwa dla moduÅu"/>
    <label for="module-station">Stacja:</label>
    <select id="module-station">
    <?php
      $sql = $pdo->query("SELECT id,name FROM stations");
      $showStations = "";
      foreach($sql as $row) $showStations .= '<option value="'.$row['id'].'">'.ucfirst($row['name']).'</option>';
      echo $showStations;
    ?>
    </select>
    <label for="module-part">CzÄÅÄ:</label>
    <select id="module-part">
    <?php
      $sql = $pdo->query("SELECT id,name,type FROM parts");
      $showParts = "";
      foreach($sql as $row) $showParts .= '<option value="'.$row['id'].'">'.ucfirst($row['name']).' - '.$row['type'].'</option>';
      echo $showParts;
    ?>
    </select>
    <label for="module-pin">PIN:</label>
    <input type="text" id="module-pin" placeholder="Nazwa pinu z pÅytki"/>
    <button class="tertiary" onClick="addModule();">Dodaj</button>
  </div>
</div>
</html>
