<!DOCTYPE html>
<head>
  <?php include('elements/head.php'); ?>
</head>
<body style="background: url('pages/cubes.png');">
  <div class="container">
    <a class="button primary" href="settings">Ustawienia</a>
    <?php
      $Database = Database::getInstance();
  		$pdo = $Database->getPDO();
      $sql = $pdo->query("SELECT * FROM stations");
      if($sql->rowCount() > 0){
        foreach($sql as $row){
          $showStation = '<div class="row">';
          $Station = new Station;
          $Station->id = $row['id'];
          $Station->name = $row['name'];
          $Station->ip = $row['ip'];
          $Station->port = $row['port'];
          if($Station->checkAvailability()) $showStation .= '<div class="col-sm-2"><h2>'.ucfirst($Station->name).'</h2><iframe name="'.$Station->name.'" src="http://'.$Station->ip.':'.$Station->port.'"></iframe></div>';
          else {
            $showStation .= '<p>Stacja <strong>'.ucfirst($Station->name).'</strong> nie jest dostÄpna.</p></div>';
            echo $showStation;
            continue;
          }
          $showStation .= '</div>';
          echo $showStation;
        }
      } else echo 'Brak stacji w bazie danych.';
    ?>
  </div>
</body>
</html>
