<?php
DEFINE('WHOIS','SYSTEM');
chdir("../");
include('config.php');
ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
if($_POST['settings'] == 'station'){
  $Database = Database::getInstance();
  $pdo = $Database->getPDO();
  $sql = $pdo->query("SELECT * FROM stations WHERE id='".htmlspecialchars($_POST['id'])."'");
  foreach($sql as $row){
    $data['id'] = $row['id'];
    $data['name'] = $row['name'];
    $data['ip'] = $row['ip'];
    $data['port'] = $row['port'];
  }
  $sql->closeCursor();
  echo json_encode($data,JSON_PRETTY_PRINT);
} elseif($_POST['settings'] == 'module'){
  $Database = Database::getInstance();
  $pdo = $Database->getPDO();
  $sql = $pdo->query("SELECT * FROM modules WHERE id='".htmlspecialchars($_POST['id'])."'");
  foreach($sql as $row){
    $data['id'] = $row['id'];
    $data['name'] = $row['name'];
    $station = $row['station'];
    $pin = $row['pin'];
  }
  $Module = new Module;
  $Module->station = $station;
  $sqlPINS = $pdo->query("SELECT pin,name FROM pins WHERE board='".htmlspecialchars($Module->stationBoard())."'");
  $data['pinsHTML'] = "";
  foreach($sqlPINS as $rowPINS){
    $data['pinsHTML'] .= $pin==$rowPINS['pin'] ? '<option value="'.$rowPINS['pin'].'" selected>'.$rowPINS['name'].'</option>' : '<option value="'.$rowPINS['pin'].'">'.$rowPINS['name'].'</option>';
  }
  echo json_encode($data,JSON_PRETTY_PRINT);
}

?>
