<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
$response = array(
  'emptyData'=>array(
    'type'=>'error',
    'message'=>'Empty data.',
    'elements'=>'all'
  ),
  'error'=>array(
    'type'=>'error',
    'message'=>'Unkown error.',
    'elements'=>'all'
  ),
  'success'=>array(
    'type'=>'success',
    'message'=>'All ok.',
    'elements'=>'all'
  ),
  'busyType'=>array(
    'type'=>'error',
    'message'=>'At this station is module with this type of part.',
    'elements'=>'part'
  ),
  'busyPIN'=>array(
    'type'=>'error',
    'message'=>'At this station is module with this PIN.',
    'elements'=>'pin'
  ),
  'validPIN'=>array(
    'type'=>'error',
    'message'=>'This PIN is valid for this board of station.',
    'elements'=>'pin'
  ),
  'busyName'=>array(
    'type'=>'error',
    'message'=>'This name is already use.',
    'elements'=>'name'
  )
);

DEFINE('WHOIS','SYSTEM');
chdir("../");
include('config.php');

if($_POST['action'] == 'add'){
  if($_POST['name'] != "" && $_POST['station'] != "" && $_POST['part'] != "" && $_POST['pin'] != ""){
    $Database = Database::getInstance();
    $pdo = $Database->getPDO();
    $Module = new Module;
    $Module->name = $_POST['name'];
    $Module->station = $_POST['station'];
    $Module->part = $_POST['part'];
    $checkName = $pdo->query("SELECT id FROM modules WHERE station='".htmlspecialchars($Module->station)."' AND name='".htmlspecialchars($Module->name)."'");
    if($checkName->rowCount() > 0){
      echo json_encode($response['busyName']);
      exit;
    }
    $checkPart = $pdo->query("SELECT type FROM parts WHERE id='".htmlspecialchars($Module->part)."'");
    $partType = $checkPart->fetch()['type'];
    $searchforpart = $pdo->query("SELECT part FROM modules WHERE station='".htmlspecialchars($Module->station)."'");
    foreach($searchforpart as $row){
      $searchfortype = $pdo->query("SELECT type FROM parts WHERE id='".$row['part']."'");
      $checkType = $searchfortype->fetch()['type'];
      if($partType == $checkType) {
        echo json_encode($response['busyType']);
        exit;
      }
    }
    $searchforpin = $pdo->query("SELECT pin FROM pins WHERE board='".$Module->stationBoard()."' AND name='".htmlspecialchars(strtoupper($_POST['pin']))."'");
    if($searchforpin->rowCount() > 0) $Module->pin = $searchforpin->fetch()['pin'];
    else {
      echo json_encode($response['validPIN']);
      exit;
    }
    $checkpin = $pdo->query("SELECT id FROM modules WHERE station='".$Module->station."' AND pin='".$Module->pin."'");
    if($checkpin->rowCount() > 0) echo json_encode($response['busyPIN']);
    else {
      if($Module->new()) echo json_encode($response['success']);
      else echo json_encode($response['error']);
    }
  } else echo json_encode($response['emptyData']);
} elseif($_POST['action'] == 'edit'){
  if($_POST['name'] != "" && $_POST['pin'] != ""){
    $Database = Database::getInstance();
    $pdo = $Database->getPDO();
    $checkName = $pdo->query("SELECT id,station FROM modules WHERE name='".htmlspecialchars($_POST['name'])."'");
    if($checkName->rowCount() > 0 AND $checkName->fetch()['id'] != $_POST['id']){
      echo json_encode($response['busyName']);
    } else {
      $checkPIN = $pdo->query("SELECT id FROM modules WHERE station='".$checkName->fetch()['station']."' AND pin='".htmlspecialchars($_POST['pin'])."'");
      if($checkPIN->rowCount() > 0 AND $checkPIN->fetch()['id'] != $_POST['id']){
        echo json_encode($response['busyPIN']);
      } else {
        $Module = new Module;
        $Module->id = $_POST['id'];
        $Module->name = $_POST['name'];
        $Module->pin = $_POST['pin'];
        if($Module->update()){
          echo json_encode($response['success']);
        } else echo json_encode($response['error']);
      }
      $checkPIN->closeCursor();
    }
    $checkName->closeCursor();
  } else echo json_encode($response['emptyData']);
} else echo json_encode($response['error']);
?>
