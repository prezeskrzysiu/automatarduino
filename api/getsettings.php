<?php
ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
header('Content-Type: application/json');

$espIP = $_SERVER['REMOTE_ADDR'];
DEFINE('WHOIS','ESP');
chdir("../");
include('config.php');
$Database = Database::getInstance();
$pdo = $Database->getPDO();
$checkIP = $pdo->query("SELECT id FROM stations WHERE ip='".htmlspecialchars($espIP)."'");
if($checkIP->rowCount() == 1){
  $stationID = $checkIP->fetch()['id'];
  $checkModules = $pdo->query("SELECT * FROM modules WHERE station='".htmlspecialchars($stationID)."' AND enable=1");
  if($checkModules->rowCount() > 0){
    $json = array();
    $i = 1;
    foreach($checkModules as $row){
      $searchfortype = $pdo->query("SELECT type FROM parts WHERE id='".htmlspecialchars($row['part'])."'");
      $moduleType = $searchfortype->fetch()['type'];
      $json[$i]['id'] = $row['id'];
      $json[$i]['name'] = $row['name'];
      $json[$i]['pin'] = $row['pin'];
      $json[$i]['type'] = $moduleType;
      $i++;
    }
    echo json_encode($json,JSON_PRETTY_PRINT);
  } else echo json_encode('Brak modulow przypisanych do stacji, badz zaden z nich nie jest wlaczony.',JSON_PRETTY_PRINT);
} else echo json_encode('Nie ma takiej stacji w bazie',JSON_PRETTY_PRINT);


?>
