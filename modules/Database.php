<?php

class Database {
  private $pdo = null;
  private static $__instance = null;

  private function __construct() {
    try {
      $this->pdo = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASSWORD);
      $this->pdo->exec("SET NAMES utf-8");
      $stmt = $this->pdo->prepare("SET @@time_zone = :timeZone");
      $timeZone = ini_get("date.timezone");
      $stmt->bindParam(":timeZone", $timeZone, PDO::PARAM_STR);
      $stmt->execute();
      unset($stmt);
    } catch(PDOException $e) {
      error_log("DB_ERROR @ ".__FILE__.": caught PDOException #".$e->getCode().": ".$e->getMessage());
    }
  }

  public static function getInstance() {
    if(self::$__instance == null) self::$__instance = new Database();
    return self::$__instance;
  }

  public function getPDO() {
    return $this->pdo;
  }
}

?>
