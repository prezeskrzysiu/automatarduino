<?php

class Module {
  public $id;
  public $name;
  public $station;
  public $part;
  public $pin;
  public $enable;

  public function new(){
    $Database = Database::getInstance();
		$pdo = $Database->getPDO();
    $addStation = $pdo->query("INSERT INTO modules(name,station,part,pin,enable) VALUES('".htmlspecialchars($this->name)."','".htmlspecialchars($this->station)."','".htmlspecialchars($this->part)."','".htmlspecialchars($this->pin)."',1)");
    return $addStation ? true : false;
  }

  public function update(){
    $Database = Database::getInstance();
		$pdo = $Database->getPDO();
    $updateModule = $pdo->query("UPDATE modules SET name='".htmlspecialchars($this->name)."',pin='".htmlspecialchars($this->pin)."' WHERE id='".htmlspecialchars($this->id)."'");
    return $updateModule ? true : false;
  }

  public function stationName(){
    $Database = Database::getInstance();
		$pdo = $Database->getPDO();
    $sql = $pdo->query("SELECT name FROM stations WHERE id='".htmlspecialchars($this->station)."'");
    return $sql->fetch()['name'];
  }

  public function stationBoard(){
    $Database = Database::getInstance();
		$pdo = $Database->getPDO();
    $sql = $pdo->query("SELECT board FROM stations WHERE id='".htmlspecialchars($this->station)."'");
    return $sql->fetch()['board'];
  }

  public function partName(){
    $Database = Database::getInstance();
		$pdo = $Database->getPDO();
    $sql = $pdo->query("SELECT name FROM parts WHERE id='".htmlspecialchars($this->part)."'");
    return $sql->fetch()['name'];
  }

  public function partType(){
    $Database = Database::getInstance();
		$pdo = $Database->getPDO();
    $sql = $pdo->query("SELECT type FROM parts WHERE id='".htmlspecialchars($this->part)."'");
    return $sql->fetch()['type'];
  }

  public function pinName(){
    $Database = Database::getInstance();
		$pdo = $Database->getPDO();
    $sql = $pdo->query("SELECT name FROM pins WHERE board='".htmlspecialchars($this->stationBoard())."' AND pin='".htmlspecialchars($this->pin)."'");
    return $sql->fetch()['name'];
  }
}

?>
